<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Skyup test case</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    {{--<link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
</head>
<body>
<div id="app">
    <router-view/>
</div>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

