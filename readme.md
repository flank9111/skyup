## Skyup test case

For start this app, follow some simple steps:
- clone this repo
- run `composer update`
- fill .env file with values of your environment
- run `php artisan migrate --seed`
- run `npm install && npm run production`