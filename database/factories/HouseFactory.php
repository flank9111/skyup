<?php

use Faker\Generator as Faker;

$factory->define(\App\House::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'bedrooms' => $faker->numberBetween(1,4),
        'bathrooms' => $faker->numberBetween(1,4),
        'storeys' => $faker->numberBetween(1,2),
        'garages' => $faker->numberBetween(1,2),
        'price' => $faker->numberBetween(200000,600000),
    ];
});
